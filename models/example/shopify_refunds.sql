SELECT
	TRIM(BOTH '"' FROM CAST(refund -> 'id' as text))::bigint as transaction_id,
	TRIM(BOTH '"' FROM CAST(refund -> 'order_id' as text))::bigint as order_id,
	trim(BOTH '"' FROM CAST(refund -> 'amount' AS text))::float * 100 as amount,
	trim(BOTH '"' FROM CAST(refund -> 'status' AS text)) as status,
	TRIM(BOTH '"' FROM CAST(refund -> 'currency' AS text)) as currency,
	TRIM(BOTH '"' FROM CAST(refund -> 'processed_at' AS text)) as processed_at
FROM (
	SELECT
		(_airbyte_data ->> 'transactions')::json -> 0 AS refund
	FROM
		_airbyte_raw_shopify_order_refunds) t